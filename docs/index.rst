.. Scientific Music Theory documentation master file, created by
   sphinx-quickstart on Mon Jan  3 18:09:16 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

A Scientific Music Theory
============================

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Table of Contents

   introduction

   physics_of_sound/*
   physics_of_listening/*



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
